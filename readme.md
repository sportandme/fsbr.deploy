FSBR Deploy
============================
1. Генерируем shh-ключ ```$ ssh-keygen -m PEM -t rsa -b 2048 -C "your_email@sample.ru"```
2. Сохраняем ключ в папку `files`
3. Прописываем путь до ключа в `ansible.cfg`, параметр `private_key_file`<br>
> Если возникли на дальнейших этапах, то возможно стоит положить ключ в директорию текущего пользователя
`~/.ssh`
4. Через админку хостинг-провайдера добавляем публичную часть ключа в настройках сервера.<br>
В файле `vars/all.yml` указываем параметры окружения
    - `mysql.database`, `mysql.root_password` (32 символа), `mysql.password` (32 символа)
    - `redis.password` (32 символа), `redis.port` (4000-5000)
    - `nginx.docroot` (путь к папке `web` проекта), `nginx.servername` (совпадает с доменом)
    - `app.project_root` (путь к папке с проектом)
5. Прописываем в файле `hosts` ip-адрес, имя сервера, пользователя, путь до ключа
6. В файле `playbook.yml` в качестве параметра `hosts` прописываем имя сервера заданное на предыдущем шаге
7. Ставим ansible
> Для винды установка возможна только из под [wsl](https://docs.microsoft.com/ru-ru/windows/wsl/install-win10)
```
$ sudo apt-get update
$ sudo apt-get install python-pip git libffi-dev libssl-dev -y
$ pip install ansible
```
8. Запускаем playbook ```$ ansible-playbook playbook.yml```
> Дополнительные параметры:<br>
> С указанием файла hosts ```$ ansible-playbook playbook.yml -i hosts```<br>
> С запросом пароля ```$ ansible-playbook playbook.yml -kK```
- После успешной выкатки создаём реп и пушим всё для истории
9. Подключаемся к серверу по ssh ```$ ssh root@host -i /path/to/key/id_rsa```
10. Генерируем ssh-ключ, который будет использоваться для деплоя
```
$ ssh-keygen
$ eval `ssh-agent`
$ ssh-add ~/.ssh/id_rsa
```
11. Копируем ssh-ключ ```$ cat ~/.ssh/id_rsa.pub```
12. Добавляем ключ в список ключей проекта, который собираемся выкатывать
```
sample.site => Settings => Access keys => Add key
```
13. Возвращаемся на сервер и чекаем доступ к репозиторию ```$ ssh -T git@bitbucket.org ```
- Если всё ок, клонируем реп, прописываем в конфигах все параметры окружения, ставим зависимости через `composer` и накатываем миграции

Если нет рут доступа:
------------
1. Создаем юзера для деплоя (предварительно сгенерировать пароль), на все вопросы системы оставляем поля пустыми
```$ sudo adduser deployer```
2. Добавляем его в группу `sudo`, чтобы можно было вызывать одноимённую команду ```$ sudo gpasswd -a deployer sudo```
3. Также надо добавить в группу `www-data`, чтобы `nginx` и `php` нормально могли достучаться `$ sudo adduser deployer www-data`
4. Создаём папку, если ещё не создали ```$ sudo mkdir /var/www/sample.local```
5. Выставляем владельца и группу ```$ sudo chown -R deployer:www-data /var/www/sample.local```
6. Даём права на запись и чтение ```$ sudo chmod -R g+rw /var/www/sample.local```
7. Выставляем наследование прав ```$ sudo find /var/www/sample.local -type d -print0 | sudo xargs -0 chmod g+s```
>Когда `deployer` будет деплоить новые релизы, то всё что мы задавали выше подцепится автоматом
8. Заходим под нашим новым пользователем `deployer` (Пароль потребуется) ```$ su - deployer```
9. Создаём папку `.ssh`, если ещё не создана и задаем права
```
$ mkdir /home/$USER/.ssh
$ chmod 700 /home/$USER/.ssh
$ sudo cp /home/assk/.ssh/authorized_keys /home/$USER/.ssh/authorized_keys
$ sudo chown -R $USER:$USER /home/$USER/.ssh
$ sudo chmod 600 /home/$USER/.ssh/authorized_keys
```
10. Генерируем ключ для деплоя
```
$ ssh-keygen
$ eval ssh-agent (eval `ssh-agent -s`)
$ ssh-add ~/.ssh/id_rsa
$ cat ~/.ssh/id_rsa.pub
```
11. Добавляем ключ в список ключей проекта, который собираемся выкатывать
```
sample.site => Settings => Access keys => Add key
```
12. Возвращаемся на сервер и чекаем доступ к репозиторию ```$ ssh -T git@bitbucket.org```
- Если всё ок, клонируем реп, прописываем в конфигах все параметры окружения, ставим зависимости через `composer` и накатываем миграции
